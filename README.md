# DevOps Exercise

## AWS CREDENTIALS
The credentials are configure to be stored in **$HOME/.aws/credentials** file which is created with **aws configure** command.
## RUN 

1.  ``` packer build packer/nginx.json```
2.  ```terraform plan```
3.  ```terraform apply```

## OUTPUT
1. The output of the terraform command will be the DNS-NAME of the ELB and the address of the RDS db.
2. Terraform will automatically pick the AMI ID generated by packer.

## TOOLS

The tools being used in this exercise are:
1. Packer
2. Terraform
3. Ansible

### Packer: 
I have used it to configure the Amozon AMI beforehand by running the ansible script before the EC2 instance is up. Every new deployment can be a new ami and be versioned, rollback can also be easy with just spinning a new instance with old AMI.

few things to consider:
1. environment variable **AMI_NAME_PREFIX** have to be set to provide AMI prefix name like *nginx* 


```

    "variables": {
        "ami_name_prefix": "{{env `AMI_NAME_PREFIX`}}"
    },
```    
2. json block below runs ansible (path needs to be correct)
```
    "provisioners": [
        {
          "type": "ansible",
          "playbook_file": "../nginx.yml"
        }
    ]
 }
```

### Terraform
I have used it to provision the following components:
1. VPC
2. a Private subnet
3. ELB listening on port 80
4. EC2 instance running nginx
5. RDS instance running postgres
6. security group to connect EC2 to RDS 

**NOTE**: env variable **AMI_NAME_PREFIX** must match with the value of the following terraform variable:
```
variable "AMI-name" {
  default = "nginx-*"
}
``` 


---

*Arpit Srivastava*