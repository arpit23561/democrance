output "lb_dns_name" {
  description = "The DNS name of the load balancer."
  value       = "${aws_elb.DevOps-Demo-ELB.dns_name}"
}

output "db" {
  value = "${aws_db_instance.DevOps-demo-db.address}"
}
