# get the AMI ID
data "aws_ami" "packerGenerated" {
  most_recent      = true

  filter {
    name   = "name"
    values = ["${var.AMI-name}"]
  }
}


# Configure the AWS Provider
provider "aws" {
  version                 = "~> 2.0"
  region                  = "eu-west-1"
  shared_credentials_file = "$HOME/.aws/credentials"
}


# Create a VPC
resource "aws_vpc" "DevOps-Demo-VPC" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "DevOps-Demo-VPC"
  }
}

# private subnet for RDS
resource "aws_subnet" "DevOps-Private-Subnet" {
  vpc_id     = "aws_vpc.DevOps-Demo-VPC.id"
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "DevOps-Demo-Subnet"
  }
  depends_on = [
    "aws_vpc.DevOps-Demo-VPC"
  ]
}

# public subet for ec2
resource "aws_subnet" "DevOps-Public-Subnet" {
  vpc_id     = "aws_vpc.DevOps-Demo-VPC.id"
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "DevOps-Demo-Subnet"
  }
  depends_on = [
    "aws_vpc.DevOps-Demo-VPC"
  ]
}

# security group for rds
resource "aws_security_group" "postgres-sg" {
  name   = "rds-postgres-security-group"
  vpc_id = "aws_vpc.DevOps-Demo-VPC.id"
  ingress {
    protocol    = "tcp"
    from_port   = 5432
    to_port     = 5432
    cidr_blocks = ["${aws_instance.nginx.id}"]
  }
}


# security group for elb
resource "aws_security_group" "elb-sg" {
  name   = "elb-security-group"
  vpc_id = "aws_vpc.DevOps-Demo-VPC.id"
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# nginx instance 
resource "aws_instance" "nginx" {
  ami           = "${data.aws_ami.packerGenerated.id}"
  instance_type = "${var.instancetype}"
  key_name      = "${var.key_name}"
  subnet_id     = "${aws_subnet.DevOps-Public-Subnet}"

  tags = {
    Name = "DevOps-Demo-Nginx"
  }
}


# Create a new load balancer
resource "aws_elb" "DevOps-Demo-ELB" {
  name               = "DevOps-Demo-ELB"
  availability_zones = ["${var.availablity-zone}"]
  subnets            = ["${aws_subnet.DevOps-Public-Subnet}"]
  security_groups    = ["${aws_security_group.elb-sg}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  instances                   = ["${aws_instance.nginx.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "DevOps-Demo-ELB"
  }
}


# RDS instance
resource "aws_db_instance" "DevOps-demo-db" {
  allocated_storage     = 8
  max_allocated_storage = 100
  storage_type          = "gp2"
  engine                = "PostgreSQL"
  engine_version        = "11.6"
  instance_class        = "db.t2.micro"
  name                  = "${var.db-name}"
  username              = "${var.db-user}"
  password              = "${var.db-password}"
  parameter_group_name  = "default.PostgreSQL11.6"
  vpc_security_group_ids = ["${aws_security_group.postgres-sg.id}"]
  db_subnet_group_name   = "${aws_subnet.DevOps-Private-Subnet}"
}