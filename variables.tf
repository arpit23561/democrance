variable "key_name" {
  default = "democrance.pem"
}

variable "availablity-zone" {
  default = "us-west-1a"
}

variable "instancetype" {
  default = "t2.micro"
}

variable "AMI-name" {
  default = "nginx-*"
}

variable "db-name" {
  default = "democrance"
}

variable "db-user" {
  default = "democrance"
}

variable "db-password" {
  default = "democrance"
}
